
import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as mpl
import sys


# Identificamos las casillas blancas y sus respectivas coordenadas

def identify_white(image):
    cordinate_list = []
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            pixelColor = (image[i, j, 0], image[i, j, 1], image[i, j, 2])
            # Aqui identificamos las casillas blancas en orden descendente
            if (pixelColor == (1,1,1)):
                cordinate_list += [[i,j]]
    return cordinate_list

# Identificamos la frontera para definir el sistema de ecuaciones
#   - A - 
#   D u B
#   - C -

def a_b_fill(U, image):
    N = len(U)
    A = csc_matrix((N, N))
    b = np.zeros((N,3))
    for n in range(0,N):
        i , j = U[n][0] , U[n][1]
        A[n, U.index([i, j])] = -4
        if ([i+1, j] not in U): # A
            b[n,0] += -image[i+1, j,0] 
            b[n,1] += -image[i+1, j,1] 
            b[n,2] += -image[i+1, j,2]
        else:
            A[n, U.index([i+1, j])] = 1
        if ([i, j+1] not in U): # B
            b[n,0] += -image[i, j+1,0] 
            b[n,1] += -image[i, j+1,1] 
            b[n,2] += -image[i, j+1,2]
        else:
            A[n, U.index([i, j+1])] = 1
        if ([i-1, j] not in U): # C
            b[n,0] += -image[i-1, j,0] 
            b[n,1] += -image[i-1, j,1] 
            b[n,2] += -image[i-1, j,2]
        else:
            A[n, U.index([i-1, j])] = 1
        if ([i, j-1] not in U): # D
            b[n,0] += -image[i, j-1,0] 
            b[n,1] += -image[i, j-1,1] 
            b[n,2] += -image[i, j-1,2]
        else:
            A[n, U.index([i, j-1])] = 1
    return [A,b]

# Una vez resolvemos el sistema aplicamos los valores correspondientes

def image_fixer(image, r, g, b, broken_pixels):
    bp_len = len(broken_pixels)
    for bp in range(0,bp_len):
        i , j = broken_pixels[bp][0] , broken_pixels[bp][1]
        image[i, j, 0] = r[bp]
        image[i, j, 1] = g[bp] 
        image[i, j, 2] = b[bp]
    return image

# Donde hacemos magia

# IMPORTANTE: Si bien la finalidad de la tarea es guardar la imagen resultante
# por fines ilustrativos habilité que se muestre la imagen arreglada en pantalla
# para una desmostración eficiente del video.


    # Tomamos la imagen
broken_image = mpl.imread(sys.argv[1])
    # Tomamos el nombre de la imagen resultante
output_name = sys.argv[2]
    # Identificamos pixeles blancos
white_pixels = identify_white(broken_image)
    # Generamos las matrices A y B para RGB
info = a_b_fill(white_pixels, broken_image) # info = [A, b_rgb]
    # Resolvemos el sistema para cada color
info_r = spsolve(info[0], info[1][:,0])
info_g = spsolve(info[0], info[1][:,1])
info_b = spsolve(info[0], info[1][:,2])
    # Actualizamos los pixeles que originalmente eran blancos
fixed_image = image_fixer(broken_image,info_r, info_g, info_b,white_pixels)
    # Voilà, aquí dejamos guardada la imagen
mpl.axis("off")
mpl.imshow(fixed_image)
mpl.savefig(output_name + ".png")
