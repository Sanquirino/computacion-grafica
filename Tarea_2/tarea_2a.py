
import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import time

import transformations2 as tr2
import basic_shapes as bs
import scene_graph2 as sg
import easy_shaders as es

from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame

class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.show_route = False
        self.normal_view = True # Camera 1
        self.follow_boat = False # Camera 2
        self.from_mountain = False # Camera 3
        self.god_view = False # Camera 4


controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_L:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

    elif key == glfw.KEY_1:
        controller.normal_view = True
        controller.follow_boat = False
        controller.from_mountain = False
        controller.god_view = False
        print('Cámara Superior, Perspectiva.')

    elif key == glfw.KEY_2:
        controller.normal_view = False
        controller.follow_boat = True
        controller.from_mountain = False
        controller.god_view = False
        print('Cámara de Seguimiento, Perspectiva.')

    elif key == glfw.KEY_3:
        controller.normal_view = False
        controller.follow_boat = False
        controller.from_mountain = True
        controller.god_view = False
        print('Cámara desde la Montaña y Seguimiento, Perspectiva.')

    elif key == glfw.KEY_4:
        controller.normal_view = False
        controller.follow_boat = False
        controller.from_mountain = False
        controller.god_view = True
        print('Cámara "The God´s view", Ortográfica.')

    elif key == glfw.KEY_SPACE:
        controller.show_route = not controller.show_route

    else:
        print('Unknown key')

# Codigos de trayectoria

def read(txt):
    points = []
    with open(txt, 'r') as f :
        for line in f.readlines():
            aux = line.split(',')
            x = float(aux[0])
            y = float(aux[1])
            points += [[x,y]]
    return points

def generateT(t):
    return np.array([[1, t, t**2, t**3]]).T

def evalCurve_2d_range(M, N, start, end):
    # The parameter t should move between 0 and 1
    ts = np.linspace(start, end, N)

    # The computed value in R3 for each sample will be stored here
    curve = np.ndarray(shape=(N, 2), dtype=float)
    for i in range(len(ts)):
        T = generateT(ts[i])
        curve[i, 0:3] = np.matmul(M, T).T
    curve = curve[:-1]
    return curve

def catmull_rom(p1, p2, p3, p4, p5):
    N = 80
    c1 = evalCurve_2d_range(catmull_matrix(p1, p2, p3, p4), N, 0, 1)
    c2 = evalCurve_2d_range(catmull_matrix(p2, p3, p4, p5), N, 0, 1)
    return np.concatenate((c1, c2))

def catmull_matrix(p1, p2, p3, p4):
    G = np.concatenate((p1, p2, p3, p4), axis=1)

    Mc = 0.5 * np.array([[0, -1, 2, -1], [2, 0, -5, 3], [0, 1, 4, -3], [0, 0, -1, 1]])
    return np.matmul(G, Mc)

def fix_data(curve):
    max_x, min_x, max_y, min_y = get_max_min(curve)
    dist_x = max_x - min_x
    dist_y = max_y - min_y
    curve[:, 0] *= (2 / dist_x)
    curve[:, 1] *= (2 / dist_y)

    nmaxx, nminx, nmaxy, nminy = get_max_min(curve)
    curve[:, 0] -= (nminx + 1)
    curve[:, 1] -= (nminy + 1)
    return curve

def get_max_min(curve):
    ejex = curve[:, 0]
    ejey = curve[:, 1]
    max_x = ejex.max()
    min_x = ejex.min()
    max_y = ejey.max()
    min_y = ejey.min()
    return max_x, min_x, max_y, min_y

def createRoute(p):
    N = 50
    i = 0
    C1 = np.array([p[i]]).T
    C2 = np.array([p[i+1]]).T
    C3 = np.array([p[i+2]]).T
    C4 = np.array([p[i+3]]).T
    route = evalCurve_2d_range(catmull_matrix(C1, C2, C3, C4), N, 0, 1)
    i += 1
    while i <= len(p)-4:
        C1 = np.array([p[i]]).T
        C2 = np.array([p[i+1]]).T
        C3 = np.array([p[i+2]]).T
        C4 = np.array([p[i+3]]).T
        new_route = evalCurve_2d_range(catmull_matrix(C1, C2, C3, C4), N, 0, 1)
        route = np.concatenate((route, new_route))
        i += 1
    return route

# Codigos para escena

def createBodyTexture(img):
    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions        colors
    #   Vertices superficie
        0 , 1, 0.5, 0 , 1,
        0.3, 0.5, 0.5, 0.3, 0.5,
        0.6, 0.2, 0.5, 0.6, 0.2,
        0.3, -0.7, 0.5, 0.3, -0.7, 
        -0.3, -0.7, 0.5, -0.3, -0.7,
       -0.6, 0.2, 0.5, -0.6, 0.2,
        -0.3, 0.5, 0.5, -0.3, 0.5,
    #   Vertices base
        0, 0.9, -0.25, 0, 0.9,
        0.3, 0.5, -0.25, 0.3, 0.5,
        0.5, 0.2, -0.25, 0.5, 0.2,
        0.2, -0.6, -0.25, 0.2, -0.6,
        -0.2, -0.6, -0.25, -0.2, -0.6,
        -0.5, 0.2, -0.25, -0.5, 0.2,
        -0.3, 0.5, -0.25, -0.3, 0.5,
    #   Vertice para el centro de la superficie
        0, 0, 0.5, 0, 0,
        0, 0, -0.25, 0, 0
            ]

        # Defining connections among vertices
        # We have a triangle every 3 indices specified
    indices = [
        #   Indices cubierta
        0, 14, 1,   1, 14, 2,   2, 14, 3,   3, 14, 4,   4, 14, 5,   5, 14, 6,   6, 14, 0,
        #   Indices pared
        0, 1, 7,    7, 1, 8,    1, 2, 8,    8, 2, 9,    2, 3, 9,    9, 3, 10,   3, 4, 10,
        10, 4, 11,  4, 5, 11,    11, 5, 12,     5, 6, 12,   12, 6, 13,  6, 0, 13,   13, 0, 7,
    #   Indices base
        7, 15, 8,   8, 15, 9,   9, 15, 10,  10, 15, 11,     11, 15, 12,     12, 15, 13,     13, 15, 7
            ]

    textureFileName = img

    return bs.Shape(vertices, indices, textureFileName)

def createSailTexture(img):
    vertices = [
        -0.4, 0, 0.8, 0, 1,
        0.4, 0, 0.8, 1, 1,
        -0.4, 0.2, 0.5, 0, 0.6,
        0.4, 0.2, 0.5, 1, 0.6,
        -0.4, 0.25, 0.2, 0, 1/4,
        0.4, 0.25, 0.2, 1, 1/4,
        -0.4, 0.2, 0.1, 0, 1/8,
        0.4, 0.2, 0.1, 1, 1/8,
        -0.4, 0, 0, 0, 0,
        0.4, 0, 0, 1, 0
        ]
    indices = [
        0, 2, 1,
        1, 2, 3,
        3, 2, 4,
        3, 4, 5,
        4, 6, 5,
        5, 6, 7
        ]
    return bs.Shape(vertices, indices, img)

def createBoat():
    gpuBoat = es.toGPUShape(createBodyTexture("wood.jpg"), GL_REPEAT, GL_NEAREST)
    gpuMast = es.toGPUShape(bs.createTextureCube("wood2.jpg"), GL_REPEAT, GL_NEAREST)
    gpuSail = es.toGPUShape(createSailTexture("pirate.png"), GL_CLAMP_TO_EDGE, GL_NEAREST)
    gpuDiamond = es.toGPUShape(bs.createTextureCube("diamond.png"), GL_REPEAT, GL_NEAREST)
    gpuGold = es.toGPUShape(bs.createTextureCube("gold2.png"), GL_REPEAT, GL_NEAREST)

    boat_body = sg.SceneGraphNode("boatBody")
    boat_body.transform = tr2.uniformScale(0.1)
    boat_body.childs += [gpuBoat]

    mast = sg.SceneGraphNode("mast")
    mast.transform = tr2.matmul([tr2.scale(0.01, 0.01, 0.25),tr2.translate(0,0,0.5)])
    mast.childs += [gpuMast]

    sail = sg.SceneGraphNode("sail")
    sail.transform = tr2.matmul([tr2.uniformScale(0.15), tr2.translate(0,0,0.8)])
    sail.childs += [gpuSail]

    diamond = sg.SceneGraphNode("diamond")
    diamond.transform = tr2.matmul([tr2.uniformScale(0.02), tr2.translate(-1, -1.5, 3)])
    diamond.childs = [gpuDiamond]

    gold = sg.SceneGraphNode("gold")
    gold.transform = tr2.matmul([tr2.rotationZ(np.pi/8), tr2.uniformScale(0.02), tr2.translate(0.25, -1.5, 3)])
    gold.childs = [gpuGold]

    boat = sg.SceneGraphNode("boat")
    boat.transform = tr2.identity()
    boat.childs += [boat_body]
    boat.childs += [mast]
    boat.childs += [sail]
    boat.childs += [diamond]
    boat.childs += [gold]

    boat_rot = sg.SceneGraphNode("boat_rot")
    boat_rot.childs += [boat]

    boatMotion = sg.SceneGraphNode("boatMotion")
    boatMotion.transform = tr2.identity()
    boatMotion.childs += [boat_rot]

    return boatMotion

def createMountain():
    gpuMon_1 = es.toGPUShape(bs.createColorTriangularPyramid(218/255, 214/255, 143/255))
    gpuMon_2 = es.toGPUShape(bs.createColorTriangularPyramid(218/255, 214/255, 143/255))

    mon_1 = sg.SceneGraphNode("mon_1")
    mon_1.transform = tr2.scale(0.3, 0.3, 0.6)
    mon_1.childs += [gpuMon_1]

    mon_2 = sg.SceneGraphNode("mon_2")
    mon_2.transform = tr2.matmul([tr2.scale(0.3,0.25,0.4), tr2.translate(-0.3, 0.5, 0)])
    mon_2.childs += [gpuMon_2]
    
    mountain = sg.SceneGraphNode("mountain")
    mountain.transform = tr2.matmul([tr2.uniformScale(0.8), tr2.translate(0,0,0), tr2.rotationZ(np.pi/3)])
    mountain.childs += [mon_1]
    mountain.childs += [mon_2]

    return mountain

def createSea():
    gpuSea = es.toGPUShape(bs.createTextureQuad("sea.jpg"), GL_REPEAT, GL_NEAREST)
    gpuSky = es.toGPUShape(bs.createTextureCube("sky.jpg"), GL_REPEAT, GL_NEAREST)

    sea_scaled = sg.SceneGraphNode("sea_scaled")
    sea_scaled.transform = tr2.uniformScale(2)
    sea_scaled.childs += [gpuSea]

    sea_rotated = sg.SceneGraphNode("sea_rotated_x")
    sea_rotated.transform = tr2.identity()
    sea_rotated.childs += [sea_scaled]

    sky = sg.SceneGraphNode("sky")
    sky.transform = tr2.matmul([tr2.uniformScale(3), tr2.translate(0,0,0.4)])
    sky.childs += [gpuSky]

    sea = sg.SceneGraphNode("sea")
    sea.transform = tr2.matmul([tr2.uniformScale(1),tr2.translate(0, 0, 0)])
    sea.childs += [sea_rotated]
    sea.childs += [sky]

    return sea

def createGuide(route):

    dot = sg.SceneGraphNode("dot")
    gpuDot = es.toGPUShape(bs.createColorQuad(1,0,0))
    dot.transform = tr2.uniformScale(0.005)
    dot.childs += [gpuDot]

    guide = sg.SceneGraphNode("guide")
    dotName = "dotPos_"
    for i in range(len(route)):
        pos = int(len(route) * i / len(route))
        newNode = sg.SceneGraphNode(dotName + str(1))
        newNode.transform = tr2.translate(route[pos][0], route[pos][1], 0.001)
        newNode.childs += [dot]
        guide.childs += [newNode]

    return guide


if __name__ == "__main__":
    # Comandos para reproducir la musica
    pygame.init() # No entiendo pq lo rechaza, pero funciona. [No es parte de los alcances del curso]
    pygame.mixer.music.load("music.ogg")
    pygame.mixer.music.play(-1)

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1000
    height = 1000

    window = glfw.create_window(width, height, "Los Piratas de Voxef", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with shaders (simple, texture and lights)
    mvcPipeline = es.SimpleModelViewProjectionShaderProgram()
    textureShaderProgram = es.SimpleTextureModelViewProjectionShaderProgram()

    # Setting up the clear screen color
    glClearColor(0, 0, 1, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)
    
    # Leemos los puntos que se encuentran en el archivo trayectoria.txt
    info = read(sys.argv[1])
    # Generamos la ruta
    route = fix_data(createRoute(info))
    # Creamos la escena
    seaNode = createSea()
    boat = createBoat()
    mountain = createMountain()
    guide = createGuide(route)
    # Obtenemos los nodos para la actualización de movimiento
    boatMove = sg.findNode(boat, "boatMotion")
    boatRot = sg.findNode(boat, "boat_rot")

    while not glfw.window_should_close(window):
        # Using the same view and projection matrices in the whole application
        projection = tr2.perspective(60, float(width) / float(height), 0.1, 40)

        t1 = glfw.get_time()
        pos = int(len(route) * t1 /10) % len(route)
        if pos+1 < len(route)-1:
            dx = route[pos+1][0]- route[pos][0]
            dy = route[pos+1][1]- route[pos][1]
            alfa = np.arctan2(dy, dx)
        else:
            alfa = 0

        boatRot.transform = tr2.rotationZ(alfa - np.pi/2)
        boatMove.transform = tr2.translate(route[pos][0], route[pos][1], 0)

        x = route[pos][0]
        y = route[pos][1]

        if controller.normal_view:
            normal_view = tr2.lookAt(
                np.array([1, -1, 1]),
                np.array([0, 0, 0]),
                np.array([0, 0, 1])
            )
        elif controller.follow_boat:
            normal_view = tr2.lookAt(
                np.array([x-0.3, y- 0.3, 0.3]),
                np.array([x + 0.1 , y + 0.1, 0]), 
                np.array([0, 0, 1]) 
            )
        elif controller.from_mountain:
            normal_view = tr2.lookAt(
                np.array([-x/2, -y/2, 1]),
                np.array([x , y , 0]), 
                np.array([0, 0, 0.5]) 
            )
        elif controller.god_view:
            projection = tr2.ortho(1, -1, 1, -1, -1.1, 1.1)
            normal_view = tr2.lookAt(
                np.array([0,0,1]),
                np.array([0,0,0]),
                np.array([0,0.1,0])
            )
        
        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # Shader para objetos que no utilizan texturas (Ruta y Montañas)
        glUseProgram(mvcPipeline.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)


        if controller.show_route:
            sg.drawSceneGraphNode(guide, mvcPipeline)

        sg.drawSceneGraphNode(mountain, mvcPipeline)
        
        # Shader para objetos que utilizan texturas
        glUseProgram(textureShaderProgram.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "view"), 1, GL_TRUE, normal_view)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "model"), 1, GL_TRUE, tr2.identity())
         
        sg.drawSceneGraphNode(boat, textureShaderProgram)
        sg.drawSceneGraphNode(seaNode, textureShaderProgram)

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    glfw.terminate()