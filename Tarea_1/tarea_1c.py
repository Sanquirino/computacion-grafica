
from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph_1b as sg
import sys
import time
import pygame

INT_BYTES = 4

class Controller:
    fillPolygon = True     # Para testear el correcto diseño de los dibujos
    start = False          # Controla el inicio del movimiento
    signal = False         # Controla la emisión del Rasho Laser

controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_ESCAPE:
        sys.exit()
    
    elif key == glfw.KEY_F:
        controller.fillPolygon = not controller.fillPolygon #Dibujo
        print("Toggle GL_FILL/GL_LINE")
    elif key == glfw.KEY_ENTER:
        controller.start = not controller.start #Inicio de la carrera
        print("Start the race")
    elif key == glfw.KEY_SPACE:
        controller.signal = not controller.signal #Emision de rasho laser
        print("Rasho laser expansivo ACTIVADO")

    else:
        print('Unknown key')

#Justo y necesario
class GPUShape:
    vao = 0
    vbo = 0
    ebo = 0
    size = 0

#Codigo solo para dibujar el fondo
def drawShape(shaderProgram, shape):

    # Binding the proper buffers
    glBindVertexArray(shape.vao)
    glBindBuffer(GL_ARRAY_BUFFER, shape.vbo)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ebo)

    # Setting up the location of the attributes position and color from the VBO
    # A vertex attribute has 3 integers for the position (each is 4 bytes),
    # and 3 numbers to represent the color (each is 4 bytes),
    # Henceforth, we have 3*4 + 3*4 = 24 bytes
    position = glGetAttribLocation(shaderProgram, "position")
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
    glEnableVertexAttribArray(position)
    
    color = glGetAttribLocation(shaderProgram, "color")
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
    glEnableVertexAttribArray(color)

    # Render the active element buffer with the active shader program
    glDrawElements(GL_TRIANGLES, shape.size, GL_UNSIGNED_INT, None)


#Funcion que toma el archivo de texto, entrega cuatro arreglos y un float
# 1: Vertices del camino; 2: Vertices del camino en formato de dibujo
# 3: Indices para dibujar el camino; 4: Posiciones enemigas
# 5: largo del Mapa
def lectura(txt):
    arrFollowing = []
    arrVertex = []
    arrIndices = []
    arrEnemy = []
    i= 0
    with open(txt, 'r') as f :
        for line in f.readlines():
            aux = line.split(',')
            x = float(aux[0])
            y = float(aux[1])
            arrVertex += [ x, y, 0, 30/255, 106/255, 20/255, x , -1, 0, 7/255, 50/255, 0  ]
            if (len(aux) == 3):
                e = (aux[2] == ' e\n' or  aux[2] == ' e')
            else:
                e = False

            if e:
                arrEnemy+= [[x, y]]

            arrIndices += [i, i+1, i+3, i, i+3, i+2]
            arrFollowing += [[x , y]]
            i +=2
        f.close()
        #Quitamos los indices de sobra (ultima iteracion)
        arrIndices = arrIndices[0:len(arrIndices)-6]
        largo = arrFollowing[len(arrFollowing) -1][0] - arrFollowing[0][0]

    return [arrFollowing, arrVertex, arrIndices, arrEnemy, largo]

#Un cielo bonito
def createSky():

    gpuShape = GPUShape()

    vertexData = np.array([
        -1, -1, 0, 146/255, 23/255, 106/255,
        1, -1, 0, 146/255, 23/255, 106/255,
        1, 1, 0, 0, 3/255, 20/255,
        -1, 1, 0, 0, 3/255, 20/255
        ], dtype = np.float32)
    
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype = np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to m Vnrtox Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape
#Crea el mapa
def createLand(vertice, indice):

    gpuShape = sg.GPUShape()

    vertexData = np.array( vertice, dtype = np.float32)

    indices = np.array( indice, dtype = np.uint32)

    gpuShape.size = len(indices)

    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape 
#Crea el chasis del camion
def createChasis():
    
    gpuShape = sg.GPUShape()
    a = 0
    b = 0
    c = 0
    d = 116/255
    e = 40/255
    f = 0
    vertexData = np.array([
        -0.7, 0.2, 0, a, b, c,
        -0.2, 0.2, 0, a, b, c,
        -0.2, 0.5, 0, a, b, c,
        0.1, 0.5, 0, a, b, c,
        0.3, 0.2, 0, a, b, c,
        0.5, 0.2, 0, 116/255, 60/255, 0,
        0.6, 0, 0, d, e, f,
        0.6, -0.2, 0, 116/255, 60/255, 0,
        -0.7, -0.2, 0, a, b, c,], dtype = np.float32)
    
    indices = np.array(
        [0, 8, 5,
        5, 8, 7,
        7, 6, 5,
        4, 3, 1,
        1, 3, 2], dtype = np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to m Vnrtox Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape
#Bueno, hace un circulo, donde le das como parametros el RGB de la mitad del perimetro
# inferior (ca{RGB}), perimetro superior (car{RGB}) y el centro (c{RGB})
def createCircle(caR, caG, caB, carR, carG, carB, cR = 0, cG = 0, cB = 0):
    vert = [0, 0, 0, cR,cG,cB] #Partimos con el vertice del centro
    ind = []
    n = 100
    dTheta = 2*np.pi/n
    R = 1
    for i in range(0,n+1):
        theta = i * dTheta
        if (theta > np.pi):
            color = [caR,caG,caB]
        else:
            color = [carR,carG,carB]
        vert += [R*np.cos(theta), R*np.sin(theta), 0, color[0], color[1], color[2]]
        ind += [0, i, i+1]

    gpuShape = sg.GPUShape()

    vertexData = np.array( vert, dtype = np.float32)
    
    indices = np.array( ind , dtype = np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to m Vnrtox Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape
#Crea el sombrero del Boss
def createHat(cR,cG,cB):
    gpuShape = sg.GPUShape()
    
    vertexData = np.array([
        -0.6, 0, 0, cR, cG, cB,
        -0.35, 0.2, 0, cR, cG, cB,
        -0.25, 0.4, 0, cR, cG, cB,
         0.25, 0.4, 0, cR, cG, cB,
         0.35, 0.2, 0, cR, cG, cB,
         0.6, 0, 0, cR, cG, cB,
         0, 0, 0, cR, cG, cB
        ], dtype = np.float32)
    
    indices = np.array(
        [0, 6, 1,
         1, 6, 2,
         2, 6, 3,
         3, 6, 4,
         4, 6, 5
         ], dtype = np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to m Vnrtox Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape
#Grafo que crea el camion
def createCar():
    
    #Definimos la rueda
    rueda = sg.SceneGraphNode("rueda")
    rueda.transform = tr.uniformScale(0.2)
    rueda.childs += [createCircle(0, 0, 0, 0, 0, 0)]
    #Weas pa las ruedas pa que se vean como que giran
    centro = sg.SceneGraphNode("centro")
    centro.transform = tr.uniformScale(0.1)
    centro.childs += [createCircle(170/255, 170/255, 170/255, 170/255, 170/255, 170/255)]
    perno = sg.SceneGraphNode("perno")
    perno.transform = np.matmul(tr.uniformScale(0.05), tr.translate(-0.01,0,0))
    perno.childs += [createCircle(82/255, 82/255, 82/255, 82/255, 82/255, 82/255)]
    #The final rueda
    laRueda = sg.SceneGraphNode("laRueda")
    laRueda.childs += [rueda]
    laRueda.childs += [centro]
    laRueda.childs += [perno]
    #Instancia de rotación de las ruedas
    ruedaRot = sg.SceneGraphNode("ruedaRot")
    ruedaRot.childs = [laRueda]
    #Declaramos ruedas necesarias
    ruedaDel = sg.SceneGraphNode("ruedaDel")
    ruedaDel.transform = tr.translate(0.3,-0.2,0)
    ruedaDel.childs += [ruedaRot]

    ruedaTra = sg.SceneGraphNode("ruedaTra")
    ruedaTra.transform = tr.translate(-0.5,-0.2,0)
    ruedaTra.childs += [ruedaRot]
    #Definimos el chasis
    chasis = sg.SceneGraphNode("chasis")
    chasis.transform = tr.scale(1.2,1,0)####
    chasis.childs = [createChasis()]
    #Unificamos el CAMIONMONSTRUOOOO
    camion = sg.SceneGraphNode("camion")
    camion.childs += [chasis]
    camion.childs += [ruedaDel]
    camion.childs += [ruedaTra]

    return  camion
#Grafo que crea enemigo básico
def createEnemy():

    cuerpo = sg.SceneGraphNode("cuerpo")
    cuerpo.transform = tr.uniformScale(0.5)
    cuerpo.childs += [createCircle(1, 175/255, 200/255,210/255,5/255,70/255, 1, 190/255, 210/255)]
    pompon = sg.SceneGraphNode("pompon")
    pompon.transform = tr.uniformScale(0.15)
    pompon.childs = [createCircle(210/255,5/255, 70/255, 210/255,5/255,70/255, 210/255,5/255,70/255)]
    ojo = sg.SceneGraphNode("ojo")
    ojo.transform = tr.uniformScale(0.08)
    pomPos = sg.SceneGraphNode("pomPos")
    pomPos.transform = tr.translate(0.15, 0.55,0)
    pomPos.childs = [pompon]
    ojo.childs += [createCircle(0,0,0,0,0,0)]
    ojoDer = sg.SceneGraphNode("ojoDer")
    ojoDer.transform = tr.translate(0.18,-0.1,0)
    ojoDer.childs += [ojo]
    ojoIzq = sg.SceneGraphNode("ojoIzq")
    ojoIzq.transform = tr.translate(-0.18,-0.1,0)
    ojoIzq.childs += [ojo]
    enemy = sg.SceneGraphNode("enemy")
    enemy.childs += [cuerpo]
    enemy.childs += [ojoDer]
    enemy.childs += [ojoIzq]
    enemy.childs += [pomPos]

    return enemy
#Grafo que crea el boss
def createBoss():
    cuerpo = sg.SceneGraphNode("cuerpo")
    cuerpo.transform = tr.uniformScale(0.5)
    cuerpo.childs += [createCircle(1, 240/255,70/255,1, 240/255,70/255,1, 254/255, 157/255)]

    marco = sg.SceneGraphNode("marco")
    marco.transform = tr.uniformScale(0.1)
    marco.childs += [createCircle(0,0,0,0,0,0,0,0,0)]

    arco = sg.SceneGraphNode("arco")
    arco.transform = tr.matmul([tr.scale(0.05,0.01,0), tr.translate(0,0.01,0)])
    arco.childs += [createCircle(0,0,0,0,0,0,0,0,0)]

    lente = sg.SceneGraphNode("lente")
    lente.transform = tr.uniformScale(0.08)
    lente.childs = [createCircle(1,1,1,1,1,1,1,1,1)]

    gafa = sg.SceneGraphNode("gafa")
    gafa.transform = tr.scale(2,1.8,2)
    gafa.childs += [marco]
    gafa.childs += [lente]

    gafaDer = sg.SceneGraphNode("gafaDer")
    gafaDer.transform = tr.translate(-0.25, 0, 0)
    gafaDer.childs += [gafa]

    gafaIzq = sg.SceneGraphNode("gafaIzq")
    gafaIzq.transform = tr.translate(0.25, 0, 0)
    gafaIzq.childs += [gafa] 

    lentes = sg.SceneGraphNode("lentes")
    lentes.transform = tr.matmul([tr.uniformScale(0.8), tr.translate(0,0,0)])
    lentes.childs += [arco]
    lentes.childs += [gafaDer]
    lentes.childs += [gafaIzq]

    sombrero = sg.SceneGraphNode("sombrero")
    sombrero.transform = tr.matmul([tr.uniformScale(1), tr.translate(0,0.2,0)])
    sombrero.childs += [createHat(2/255,30/255,140/255)]

    boss = sg.SceneGraphNode("boss")
    boss.childs += [cuerpo]
    boss.childs += [lentes]
    boss.childs += [sombrero]
    return boss
#Grafo que unifica el Camino con los enemigos básicos (Mapa) y el Mapa con el Camion
#Se encarga del movimiento
def createMap(info0, info1, info2, info3, info4):

    land = sg.SceneGraphNode("land")
    land.transform = tr.identity()
    land.childs += [createLand(info1, info2)]

    enemy = sg.SceneGraphNode("enemy")
    enemy.transform = tr.uniformScale(0.08)
    enemy.childs = [createEnemy()]

    bigBoss = sg.SceneGraphNode("bigBoss")
    bigBoss.transform = tr.matmul([tr.uniformScale(0.6), tr.translate(info4 - 0.6,0,0)])
    bigBoss.childs += [createBoss()]

    car = sg.SceneGraphNode("car")
    car.transform = tr.uniformScale(0.1)
    car.childs += [createCar()]
    
    carRot = sg.SceneGraphNode("carRot")
    carRot.childs += [car]

    carTr = sg.SceneGraphNode("carTr")
    carTr.transform = tr.translate(info0[1][0] - 0.04, info0[1][1] + 0.04, 0)
    carTr.childs = [carRot]

    landInicial = sg.SceneGraphNode("landInicial")
    landInicial.childs += [land]
    enemyName = "enemyPos_"
    for i in range(len(info3)):
        newNode = sg.SceneGraphNode(enemyName + str(1))
        newNode.transform = tr.translate(info3[i][0] , info3[i][1] + 0.02, 0)
        newNode.childs += [enemy]
        landInicial.childs += [newNode]
    
    landMov = sg.SceneGraphNode("landMov")
    landMov.childs += [landInicial]
    landMov.childs += [bigBoss]

    mapGame = sg.SceneGraphNode("mapGame")
    mapGame.childs += [landMov]
    mapGame.childs += [carTr]

    return mapGame
#Funcion que obtiene la coordenada Y en función de dos puntos de un segmento y la coordenada
#X de ese momento
def nuevaPos(x_1, y_1, x_2, y_2, x):
    dx = x_2 - x_1
    dy = y_2 - y_1
    dy_dx = dy/dx
    y = dy_dx * (x - x_1) + y_1
    theta = np.arctan2(dy, dx)
    return [y, theta]
#Funcion que analiza si hay un enemigo dentro de un rango determiado del camion
def isEnemyNear(pos, arrEnemy, rango):
    for enemy in arrEnemy:
        closeXb = ((enemy[0] - rango) <= pos[0])
        closeXs = (pos[0] <= (enemy[0] + rango))
        closeYb = ((enemy[1] - rango) <= pos[1]) 
        closeYs = (pos[1] <= (enemy[1] + rango))
        if (closeXb and closeYb and closeXs and closeYs) :
            return True

    return False

if __name__ == "__main__":
    # Comandos para reproducir la musica
    pygame.init() # No entiendo pq lo rechaza, pero funciona. [No es parte de los alcances del curso]
    pygame.mixer.music.load("musica.mp3")
    pygame.mixer.music.play(-1)

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 800
    height = 800

    window = glfw.create_window(width, height, "Rápidos y Fogosos: Death Race IV", None, None)
    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Defining shaders for our pipeline
    vertex_shader = """
    #version 130
    in vec3 position;
    in vec3 color;

    out vec3 fragColor;

    void main()
    {
        fragColor = color;
        gl_Position = vec4(position, 1.0f);
    }
    """

    fragment_shader = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(fragColor, 1.0f);
    }
    """

    fragment_shader_night = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(fragColor.r * 0.2, fragColor.g * 0.2, (fragColor.b + 0.2) * 0.5, 1.0f);
    }
    """
    # Shader para elementos de SG
    shaderProgramSg = sg.basicShaderProgram()
    # Assembling the shader program (pipeline) with both shaders
    shaderProgram = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))

    # Setting up the clear screen color
    glClearColor(0.15, 0.15, 0.15, 1.0)

    #Variables necesarias
    info = lectura(sys.argv[1])
    desp = 0
    theta = 1
    i = 1
    x = info[0][i][0]
    vel = 0.01

    # Creating shapes on GPU memory
    gpuSky = createSky()

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()
        #######glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)
        # Dibujos comunes y corrientes
        glUseProgram(shaderProgram)
        drawShape(shaderProgram, gpuSky)
        
        # Aqui empiezan los dibujos que usan SG
        glUseProgram(shaderProgramSg)
        theta = -10 * glfw.get_time()
        # Creamos el mapa según los requerimientos
        mapa = createMap(info[0], info[1], info[2], info[3], info[4])
        # Obtenemos los nodos que variarán en el tiempo
        mapMovNode = sg.findNode(mapa, "landMov")
        carRotNode = sg.findNode(mapa, "carRot")
        carTrNode = sg.findNode(mapa, "carTr")
        ruedaRotNode = sg.findNode(mapa, "ruedaRot")
        # Cuando presionamos 'S' por primera vez
        if controller.start:
            # La rueda empieza a moverse
            ruedaRotNode.transform = tr.rotationZ(theta)
            # Movimiento del escenario
            if desp < (info[4] - 1): 
                mapMovNode.transform = tr.translate(-desp,0,0)
                desp += vel 
            else: # Si ya se desplazó hasta el final
                vel = 0
                mapMovNode.transform = tr.matmul([mapMovNode.transform, tr.identity()])
                print("Se te acabó la carrera bb")
                sg.drawSceneGraphNode(mapa, shaderProgramSg, tr.uniformScale(1))
                time.sleep(2)
                break
            
            #Movimiento del camion
            if (i < (len(info[0])-1)): #Mientras queden vertices para desplazarse
                if not( x < info[0][i+1][0]): #Si todavia excedemos el X del punto final del segmento
                    i += 1
                #Definimos el nuevo ángulo y componente Y del camión
                nuevo_Y_Alfa = nuevaPos(info[0][i][0], info[0][i][1], info[0][i+1][0], info[0][i+1][1], x)
                y = nuevo_Y_Alfa[0]
                alfa = nuevo_Y_Alfa[1]
            # Generamos posición para el analisis de enemigos cercanos
            carCord = [x,y - info[0][1][1]]
            # Avanzamos en X
            x += vel

            # Analisis de ambiente hostil
            # Cuando presionamos 'SPACE'
            if (controller.signal):
                if isEnemyNear(carCord, info[3], 0.5):
                    print("Enemigo piteado")
                    info[3] = info[3][1:]
                    controller.signal = False
                else:
                    print("Casi le achuntas")
                    controller.signal = False
            else:
                if isEnemyNear(carCord, info[3], 0.1):
                    print("Tajeado por el enemigo")
                    sg.drawSceneGraphNode(mapa, shaderProgramSg, tr.uniformScale(1))
                    time.sleep(3)
                    break
            # Actualizamos el ángulo y posición del camión
            carRotNode.transform = tr.rotationZ(alfa)                    
            carTrNode.transform = tr.translate( info[0][1][0] , y + 0.04, 0)
        # Dibujamos :)
        sg.drawSceneGraphNode(mapa, shaderProgramSg, tr.uniformScale(1.5))
        glfw.swap_buffers(window)
        

    glfw.terminate()